package tokens

import (
	"fmt"
	"gitlab.com/tradezone/api-gateway/pkg/logger"
	"time"

	"github.com/dgrijalva/jwt-go"
)

// JWThandlear
type JWTHandler struct {
	Sub       string
	Iss       string
	Exp       string
	Iat       string
	Aud       []string
	Role      string
	SignInKey string
	Log       logger.Logger
	Token     string
}
type CustomClaims struct {
	*jwt.Token
	Sub  string   `json:"sub"`
	Iss  string   `json:"iss"`
	Exp  float64  `json:"exp"`
	Iat  float64  `json:"iat"`
	Aud  []string `json:"aud"`
	Role string   `json:"role"`
}

func (jwthand *JWTHandler) GenerateAuthJWT() ([]string, error) {
	var (
		accessToken  *jwt.Token
		refreshToken *jwt.Token
		claims       jwt.MapClaims
	)

	accessToken = jwt.New(jwt.SigningMethodHS256)
	refreshToken = jwt.New(jwt.SigningMethodHS256)

	claims = accessToken.Claims.(jwt.MapClaims)
	claims["sub"] = jwthand.Sub
	claims["iss"] = jwthand.Iss
	claims["exp"] = time.Now().Add(time.Hour * 480).Unix()
	claims["iat"] = time.Now().Unix()
	claims["aud"] = jwthand.Aud
	claims["role"] = jwthand.Role
	access, err := accessToken.SignedString([]byte(jwthand.SignInKey))
	if err != nil {
		jwthand.Log.Error("error generating access token", logger.Error(err))
		return []string{"", ""}, err
	}
	refresh, err := refreshToken.SignedString([]byte("jwthand.SignInKey"))
	if err != nil {
		jwthand.Log.Error("error generating access token", logger.Error(err))
		return []string{"", ""}, err
	}
	return []string{access, refresh}, nil
}

func (jwthand *JWTHandler) ExtractClaims() (jwt.MapClaims, error) {
	var (
		token *jwt.Token
		err   error
	)

	token, err = jwt.Parse(jwthand.Token, func(t *jwt.Token) (interface{}, error) {
		return []byte(jwthand.SignInKey), nil
	})
	if err != nil {
		return nil, err
	}
	claims, ok := token.Claims.(jwt.MapClaims)
	if !(ok && token.Valid) {
		jwthand.Log.Error("invalid jwt token")
		return nil, err
	}
	return claims, nil
}

func ExtractClaim(tokenStr string, signinkey []byte) (jwt.Claims, error) {
	var (
		token *jwt.Token
		err   error
	)
	// fmt.Println(tokenStr)
	token, err = jwt.Parse(tokenStr, func(t *jwt.Token) (interface{}, error) {
		return signinkey, nil
	})
	if err != nil {
		return nil, err
	}

	claims, ok := token.Claims.(jwt.MapClaims)
	if !(ok && token.Valid) {
		fmt.Println("not found token")
		return nil, err
	}
	return claims, nil
}
