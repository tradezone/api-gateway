package middleware

import (
	"fmt"
	"gitlab.com/tradezone/api-gateway/api/models"
	"gitlab.com/tradezone/api-gateway/api/tokens"
	"gitlab.com/tradezone/api-gateway/config"
	"net/http"
	"strings"

	"github.com/casbin/casbin/v2"
	jwtg "github.com/dgrijalva/jwt-go"
	"github.com/gin-gonic/gin"
)

type JwtRoleAuth struct {
	enforcer   *casbin.Enforcer
	cfg        config.Config
	jwtHandler tokens.JWTHandler
}

func NewAuth(enforce *casbin.Enforcer, jwtHandler tokens.JWTHandler, cfg config.Config) gin.HandlerFunc {
	a := JwtRoleAuth{
		enforcer:   enforce,
		cfg:        cfg,
		jwtHandler: jwtHandler,
	}
	return func(ctx *gin.Context) {
		allow, err := a.CheckPermission(ctx.Request)
		fmt.Println(allow)
		if err != nil {
			v, _ := err.(*jwtg.ValidationError)
			if v.Errors == jwtg.ValidationErrorExpired {
				a.RequireRefresh(ctx)
			} else {
				a.RequirePermission(ctx)
			}
		} else if !allow {
			a.RequirePermission(ctx)
		}

	}

}

// fs
func (a *JwtRoleAuth) GetRole(r *http.Request) (string, error) {
	var (
		role   string
		claims jwtg.MapClaims
		err    error
	)
	jwtToken := r.Header.Get("Authorization")

	if jwtToken == "" {
		return "unauthorized", err
	} else if strings.Contains(jwtToken, "Basic") {
		return "unauthorized", err
	}

	a.jwtHandler.Token = jwtToken
	claims, err = a.jwtHandler.ExtractClaims()
	if err != nil {
		return "", err
	}

	if claims["role"].(string) == "authorized" {
		role = "authorized"
	} else if claims["role"].(string) == "admin" {
		role = "admin"
	} else if claims["role"].(string) == "moderator" {
		role = "moderator"
	} else {
		role = "unknown"
	}
	return role, nil

}

func (a *JwtRoleAuth) CheckPermission(r *http.Request) (bool, error) {
	user, err := a.GetRole(r)
	if err != nil {
		return false, err
	}
	fmt.Println(user)
	method := r.Method
	path := r.URL.Path
	fmt.Println(r.Method)
	fmt.Println(r.URL.Path)

	allowed, err := a.enforcer.Enforce(user, path, method)
	if err != nil {
		panic(err)
	}

	return allowed, nil
}
func (a *JwtRoleAuth) RequirePermission(c *gin.Context) {
	c.AbortWithStatus(403)
}

// RequireRefresh aborts request with 401 status
func (a *JwtRoleAuth) RequireRefresh(c *gin.Context) {
	c.JSON(http.StatusUnauthorized, models.ResponseError{
		Error: models.ServerError{
			Status:  "UNAUTHORIZED",
			Message: "Token is expired",
		},
	})
	c.AbortWithStatus(401)
}
