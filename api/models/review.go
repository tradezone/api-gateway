package models

type ReviewRes struct {
	Id          int64 `json:"id"`
	PostId      int64 `json:"post_id"`
	Rating      int    `json:"rating"`
	Description string `json:"description"`
	UserId      int64 `json:"user_id"`
}
