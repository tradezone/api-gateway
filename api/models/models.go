package models

type Error struct {
	Code        int
	Error       error
	Description string
}
type ListUserReq struct {
	Page  int64
	Limit int64
}
type VerifyResponse struct {
	Id           int64             `json:"id"`
	FirstName    string            `json:"first_name"`
	LastName     string            `json:"last_name"`
	Email        string            `json:"email"`
	Bio          string            `json:"bio"`
	PhoneNumber  string            `json:"phone_number"`
	JWT          string            `json:"jwt"`
	RefreshToken string            `json:"refresh"`
	Addresses    []AddressResponse `json:"addresses"`
}

type AddressResponse struct {
	Id       int64  `json:"id"`
	UserId   int64  `json:"user_id"`
	District string `json:"district"`
	Street   string `json:"street"`
}
type LoginRequest struct {
	Email    string `json:"email"`
	Password string `json:"password"`
}
type CustomerRes struct {
	FirstName   string `json:"firstName"`
	LastName    string `json:"lastName"`
	Bio         string `json:"bio"`
	Email       string `json:"email"`
	PhoneNumber string `json:"phoneNumber"`
	Password    string `json:"password"`
}
