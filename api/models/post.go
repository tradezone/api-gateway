package models

type CreatePost struct {
	UserId     int64  `json:"user_id"`
	Name       string  `json:"name"`
	Description string  `json:"description"`
	Medias      []Media `json:"medias"`
}

type Media struct {
	Name string `json:"name"`
	Link string `json:"link"`
	Type string `json:"type"`
}

type UpdatePost struct {
	Id          int64  `json:"id"`
	UserId     int64  `json:"user_id"`
	Name       string  `json:"name"`
	Description string  `json:"description"`
	Medias      []UpdateMedia `json:"medias"`
}

type UpdateMedia struct {
	Id     int64 `json:"id"`
	PostId int64 `json:"post_id"`
	Name   string `json:"name"`
	Link   string `json:"link"`
	Type   string `json:"type"`
}
