package v1

import (
	"fmt"
	"gitlab.com/tradezone/api-gateway/api/models"
	pbu "gitlab.com/tradezone/api-gateway/genproto/customer"
	"gitlab.com/tradezone/api-gateway/pkg/etc"
	"gitlab.com/tradezone/api-gateway/pkg/logger"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"golang.org/x/net/context"
)

// @Summary login
// @Description 	this gets LogIn
// @Tags Auth
// @Accept json
// @Produce json
// @Param 			email 		query string true "email"
// @Param 			password 	query string true "password"
// @Success 200 {object} customer.CustomerRes
// @Failure 400 {object} models.Error
// @Router /login/{email}/{password} [get]
func (h *handlerV1) LogIn(c *gin.Context) {

	var body = &models.LoginRequest{}
	c.ShouldBindJSON(body)
	email := c.Param("email")
	body.Email = email
	password := c.Param("password")
	body.Password = password

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	user, err := h.serviceManager.Customer().LogIn(ctx, &pbu.LoginRequest{Email: body.Email, Password: body.Password})
	if err != nil {
		h.log.Error("error while logging into ", logger.Error(err))

		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "Something went wrong, please try again",
		})
		return
	}
	user_id := fmt.Sprint(user.Id)
	if !etc.CheckPasswordHash(body.Password, user_id) {
		c.JSON(http.StatusNotFound, models.Error{
			Description: "Password or Email error",
			Code:        http.StatusBadRequest,
		})
		return
	}

	h.jwtHandler.Iss = "user"
	h.jwtHandler.Sub = user_id
	h.jwtHandler.Role = "authorized"
	h.jwtHandler.Aud = []string{"project-app"}
	h.jwtHandler.SignInKey = h.cfg.SignInKey
	h.jwtHandler.Log = h.log
	tokens, err := h.jwtHandler.GenerateAuthJWT()
	refreshToken := tokens[1]
	if err != nil {
		h.log.Error("error occured while generating tokens")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong,please try again",
		})
		return
	}
	user.RefreshToken = refreshToken
	user.Password = ""
	c.JSON(http.StatusOK, user)
}
