package v1

import (
	"context"
	"fmt"
	"gitlab.com/tradezone/api-gateway/api/models"
	"gitlab.com/tradezone/api-gateway/genproto/customer"
	"gitlab.com/tradezone/api-gateway/pkg/etc"
	"gitlab.com/tradezone/api-gateway/pkg/logger"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// Moderator swagegr
// @Summary			Login moderator
// @Description		Login moderator
// @Tags			Moderator
// @Accept			json
// @Produce			json
// @Param			name		path string true "moderator name"
// @Param 			password 	path string true "password"
// @Success			200 		{object} 	customer.GetModeratorRes
// @Failure			400			{object}	models.Error
// @Failure			500			{object}	models.Error
// @Failure			404			{object}	models.Error
// @Failure			409			{object}	models.Error
// @Router			/v1/moderator/login/{name}/{password} [get]
func (h *handlerV1) LoginModerator(c *gin.Context) {
	var (
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseEnumNumbers = true

	var (
		name     = c.Param("name")
		password = c.Param("password")
	)

	ctx, cancer := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancer()

	res, err := h.serviceManager.Customer().GetModerator(ctx, &customer.GetModeratorReq{Name: name})

	if err != nil {
		c.JSON(http.StatusNotFound, models.Error{
			Code:        http.StatusNotFound,
			Error:       err,
			Description: "Couln't find matching information, Have you registered before?",
		})
		h.log.Error("Error while getting admin by admin Name", logger.Any("Get", err))
		return
	}
	if !etc.CheckPasswordHash(password, res.Password) {
		c.JSON(http.StatusConflict, models.Error{
			Description: "Password or adminName error",
			Code:        http.StatusConflict,
		})
		return
	}

	if res.Password != password {
		fmt.Println("error password")
		return
	}

	h.jwtHandler.Iss = "moderator"
	h.jwtHandler.Sub = res.Id
	h.jwtHandler.Role = "moderator"
	h.jwtHandler.Aud = []string{"exam-app"}
	h.jwtHandler.SignInKey = h.cfg.SignInKey
	h.jwtHandler.Log = h.log
	tokens, err := h.jwtHandler.GenerateAuthJWT()
	accessToken := tokens[0]

	if err != nil {
		h.log.Error("error occured while generating tokens")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong,please try again",
		})
		return
	}

	res.AccessToken = accessToken
	res.Password = ""

	c.JSON(http.StatusOK, res)

}
