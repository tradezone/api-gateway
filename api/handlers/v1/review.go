package v1

import (
	"context"
	"net/http"
	"strconv"
	"time"

	"gitlab.com/tradezone/api-gateway/api/models"
	pr "gitlab.com/tradezone/api-gateway/genproto/review"
	"gitlab.com/tradezone/api-gateway/pkg/logger"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// Create review
// @Summary 		Create review
// @Description 	this creates review
// @Tags 			Review
// @Accept 			json
// @Security 		BearerAuth
// @Produce         json
// @Param           review        body  	 review.ReviewRes true "review"
// @Success         201					  {object} 	review.ReviewRes
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /reviews [post]
func (h *handlerV1) CreateReveiw(c *gin.Context) {
	var (
		body        *pr.ReviewReq
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.Error{
			Code:        http.StatusBadRequest,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while binding json", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.Review().CreateReview(ctx, body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while creting review", logger.Error(err))
		return
	}
	c.JSON(http.StatusCreated, response)
}

// Delete review by postId
// @Summary 		Delete review
// @Description 	this delete review
// @Tags 			Review
// @Accept 			json
// @Security 		BearerAuth
// @Produce         json
// @Param           ReviewPostId        body  	  review.ReviewPostId true "review"
// @Success         200					  {object} 	models.Error
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /reviews/postid/{id} [delete]
func (h *handlerV1) DeleteReviewPostId(c *gin.Context) {
	var (
		body        pr.ReviewPostId
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.Error{
			Code:        http.StatusBadRequest,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while binding json", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	_, err = h.serviceManager.Review().DeleteReviewPostId(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while deleting post review", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, models.Error{
		Error:       nil,
		Code:        http.StatusOK,
		Description: "post just has deleted",
	})
}

// Delete review by userId
// @Summary 		Delete review
// @Description 	this delete review
// @Tags 			Review
// @Accept 			json
// @Produce         json
// @Security 		BearerAuth
// @Param           ReviewUserId        body  	  review.ReviewUserId true "review"
// @Success         200					  {object} 	models.Error
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /reviews/userid/{id} [delete]
func (h *handlerV1) DeleteReviewUserId(c *gin.Context) {
	var (
		body        pr.ReviewUserId
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.Error{
			Code:        http.StatusBadRequest,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while binding json", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	_, err = h.serviceManager.Review().DeleteReviewUserId(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while deleting post review", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, models.Error{
		Error:       nil,
		Code:        http.StatusOK,
		Description: "post just has deleted",
	})
}

// Update review
// @Summary 		Update review
// @Description 	this Updates review
// @Tags 			Review
// @Accept 			json
// @Security 		BearerAuth
// @Produce         json
// @Param           review        body  	  models.ReviewRes true "review"
// @Success         200					  {object} 	review.ReviewRes
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /reviews/update [put]
func (h *handlerV1) UpdateReview(c *gin.Context) {
	var (
		body        models.ReviewRes
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.Error{
			Code:        http.StatusBadRequest,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while binding json", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	reviewSaved := &pr.ReviewRes{
		Id:          body.Id,
		PostId:      body.PostId,
		Description: body.Description,
		UserId:      body.UserId,
		Rating:      int64(body.Rating),
	}

	response, err := h.serviceManager.Review().UpdateReview(ctx, reviewSaved)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while updating review", logger.Error(err))
		return
	}

	c.JSON(http.StatusOK, response)
}

// Get review By Post Id
// @Summary 		Get review
// @Description 	this gets review
// @Tags 			Review
// @Accept 			json
// @Security 		BearerAuth
// @Produce         json
// @Param           id        query  int  true "id"
// @Success         200					  {object} 	review.Reviews
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /reviews/posts/{id} [get]
func (h *handlerV1) GetRewPostId(c *gin.Context) {
	var (
		body        pr.ReviewPostId
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	guid := c.Query("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Not found",
		})
		h.log.Error("Error while converting id in getting customer", logger.Error(err))
		return
	}
	body.PostId = id
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.Review().GetRewPostId(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while getting rewies by post id", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, response)
}

// Get review By User Id
// @Summary 		Get review
// @Description 	this gets review
// @Tags 			Review
// @Security 		BearerAuth
// @Accept 			json
// @Produce         json
// @Param           ReviewUserId        query  int  true "ReviewUserId"
// @Success         200					  {object} 	review.Reviews
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /reviews/userid/{id} [get]
func (h *handlerV1) GetRewUserId(c *gin.Context) {
	var (
		body        pr.ReviewUserId
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	guid := c.Query("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Not found",
		})
		h.log.Error("Error while converting id in getting customer", logger.Error(err))
		return
	}
	body.UserId = id
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.Review().GetRewUserId(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while getting reviews by post id", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, response)
}
