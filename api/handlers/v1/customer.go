package v1

import (
	"context"
	"net/http"
	"strconv"
	"time"

	_ "gitlab.com/tradezone/api-gateway/api/docs" //swag
	"gitlab.com/tradezone/api-gateway/api/models"
	pc "gitlab.com/tradezone/api-gateway/genproto/customer"
	"gitlab.com/tradezone/api-gateway/pkg/logger"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// Create customer
// @Summary 		Create customer
// @Description 	this is expired but still staying because of exam instead use register and verify
// @Tags 			Customer
// @Accept 			json
// @Produce         json
// @Security 		BearerAuth
// @Param           Customer        body  	customer.CustomerReq true "customer"
// @Success         201					  {object} 	customer.CustomerRes
// @Failure         500                   {object}  models.Error
// @Router          /customers [post]
func (h *handlerV1) CreateCustomer(c *gin.Context) {
	var (
		body        *pc.CustomerReq
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.Error{
			Code:        http.StatusBadRequest,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while binding json", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.Customer().CreateCustomer(ctx, body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while creating customer", logger.Error(err))
		return
	}

	c.JSON(http.StatusCreated, response)
}

// Delete customer
// @Summary 		Delete customer
// @Description 	this deletes customer
// @Tags 			Customer
// @Accept 			json
// @Produce         json
// @Security 		BearerAuth
// @Param           Id        query  	customer.CustomerId true "Id"
// @Success         201					  {object} 	models.Error
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /customers/{id} [delete]
func (h *handlerV1) DeleteCustomer(c *gin.Context) {
	var (
		body        pc.CustomerId
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	guid := c.Query("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Not found",
		})
		h.log.Error("Error while converting id in getting customer", logger.Error(err))
		return
	}
	body.Id = id
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	_, err = h.serviceManager.Customer().DeleteCustomer(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while deleting customer", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, models.Error{
		Error:       nil,
		Code:        http.StatusOK,
		Description: "Customer succesfully deleted",
	})
}

// Update customer
// @Summary 		Udate customer
// @Description 	this udates customer
// @Tags 			Customer
// @Accept 			json
// @Produce         json
// @Security 		BearerAuth
// @Param           customer        body  	customer.CustomerRes true "customer"
// @Success         200					  {object} 	customer.CustomerRes
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /customers [put]
func (h *handlerV1) UpdateCustomer(c *gin.Context) {
	var (
		body        *pc.CustomerRes
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.JSON(http.StatusBadRequest, models.Error{
			Code:        http.StatusBadRequest,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while binding json", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.Customer().UpdateCustomer(ctx, body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Check your data",
		})
		h.log.Error("Error while updating customer", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, response)
}

// Get customer
// @Summary 		Get customer
// @Description 	this gets customer
// @Tags 			Customer
// @Accept 			json
// @Produce         json
// @Security 		BearerAuth
// @Param           id        query  int  true "id"
// @Success         200					  {object} 	customer.Customer
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /customers/{id} [get]
func (h *handlerV1) GetCustomer(c *gin.Context) {
	var (
		body        pc.CustomerId
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true

	guid := c.Query("id")
	id, err := strconv.ParseInt(guid, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Not found",
		})
		h.log.Error("Error while converting id in getting customer", logger.Error(err))
		return
	}
	body.Id = id

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	response, err := h.serviceManager.Customer().GetCustomer(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Not found",
		})
		h.log.Error("Error while getting customer info", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, response)
}

// Get Customers
// @Summary   Get Customers
// @Description It gets all customers
// @Tags Customers
// @Accept json
// @Security 		BearerAuth
// @Produce json
// @Security 		BearerAuth
// @Success         200					  {object} 	customer.Customers
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /customers/all [get]
func (h *handlerV1) GetCustomers(c *gin.Context) {
	var (
		body        pc.Empty
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	response, err := h.serviceManager.Customer().GetCustomers(ctx, &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Not found",
		})
		h.log.Error("Error while getting customer info", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, response)
}

// Get Customers
// @Summary   List Customers
// @Description It gets all customers
// @Tags Customers
// @Accept json
// @Security 		BearerAuth
// @Produce json
// @Security 		BearerAuth
// @Param           page        query  int  true "page"
// @Param           limit        query  int  true "limit"
// @Success         200					  {object} 	customer.Customers
// @Failure         500                   {object}  models.Error
// @Failure         400                   {object}  models.Error
// @Router          /customers/list/{page}/{limit} [get]
func (h *handlerV1) ListCustomers(c *gin.Context) {
	var (
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseProtoNames = true
	page := c.Query("page")
	limit := c.Query("limit")
	intPage, err := strconv.ParseInt(page, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusBadRequest,
			Error:       err,
			Description: "Not found",
		})
		h.log.Error("Error while parsing page", logger.Error(err))
		return
	}
	intLimit, err := strconv.ParseInt(limit, 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusBadRequest,
			Error:       err,
			Description: "Not found",
		})
		h.log.Error("Error while parsing limit", logger.Error(err))
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	response, err := h.serviceManager.Customer().ListCustomers(ctx, &pc.ListUserReq{
		Page:  intPage,
		Limit: intLimit,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Not found",
		})
		h.log.Error("Error while getting customer info", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, response)
}

// Search Customers
// @Summary   List Customers
// @Description It gets all customers
// @Tags Customers
// @Accept json
// @Produce json
// @Security 		BearerAuth
// @Param           page         		query  int  true "page"
// @Param           limit        		query  int  true "limit"
// @Param           key        	 		query  string  true "key"
// @Param           value        		query  string  true "value"
// @Success         200					{object} 	customer.Customers
// @Failure         500                 {object}  models.Error
// @Failure         400                 {object}  models.Error
// @Router          /customers/search/{page}/{limit}/{key}/{value} [get]
func (h *handlerV1) SearchCustomer(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true
	page, err := strconv.ParseInt(c.Query("page"), 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusBadRequest,
			Error:       err,
			Description: "Not found",
		})
		h.log.Error("Error while parsing page on search api", logger.Error(err))
		return
	}
	limit, err := strconv.ParseInt(c.Query("limit"), 10, 64)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusBadRequest,
			Error:       err,
			Description: "Not found",
		})
		h.log.Error("Error while parsing limit on search api", logger.Error(err))
		return
	}
	key := c.Query("key")
	value := c.Query("value")
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()
	response, err := h.serviceManager.Customer().SearchCustomer(ctx, &pc.SearchReq{
		Key:   key,
		Value: value,
		Page:  page,
		Limit: limit,
	})
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Code:        http.StatusInternalServerError,
			Error:       err,
			Description: "Not found",
		})
		h.log.Error("Error while search all customer info", logger.Error(err))
		return
	}
	c.JSON(http.StatusOK, response)
}
