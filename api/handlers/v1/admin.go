package v1

import (
	"context"
	"fmt"
	"gitlab.com/tradezone/api-gateway/api/models"
	"gitlab.com/tradezone/api-gateway/genproto/customer"
	"gitlab.com/tradezone/api-gateway/pkg/etc"
	"gitlab.com/tradezone/api-gateway/pkg/logger"
	"net/http"
	"time"

	"github.com/gin-gonic/gin"
	"google.golang.org/protobuf/encoding/protojson"
)

// Login admin
// @Summary			Login admin
// @Description		Login admin
// @Tags			Admin
// @Accept			json
// @Produce			json
// @Param			admin_name	path string true "admin_name"
// @Param 			password 	path string true "password"
// @Success			200 		{object} 	customer.AdminCutomerReq
// @Failure			400			{object}	models.Error
// @Failure			500			{object}	models.Error
// @Failure			404			{object}	models.Error
// @Failure			409			{object}	models.Error
// @Router			/admin/login/{admin_name}/{password} [get]
func (h *handlerV1) LoginAdmin(c *gin.Context) {
	var (
		jspbMarshal protojson.MarshalOptions
	)
	jspbMarshal.UseEnumNumbers = true

	var (
		password  = c.Param("password")
		adminName = c.Param("admin_name")
	)
	fmt.Println(password, adminName)

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.Customer().GetAdmin(ctx, &customer.AdminCutomerReq{Name: adminName})

	if err != nil {
		c.JSON(http.StatusNotFound, models.Error{
			Code:        http.StatusNotFound,
			Error:       err,
			Description: "Couln't find matching information, Have you registered before?",
		})
		h.log.Error("Error while getting admin by admin Name", logger.Any("Get", err))
		return

	}

	if !etc.CheckPasswordHash(password, res.Password) {
		c.JSON(http.StatusConflict, models.Error{
			Description: "Password or adminName error",
			Code:        http.StatusConflict,
		})
		return
	}

	if res.Password != password {
		fmt.Println("error password")
		return
	}

	h.jwtHandler.Iss = "admin"
	h.jwtHandler.Sub = res.Id
	h.jwtHandler.Role = "admin"
	h.jwtHandler.Aud = []string{"exam-app"}
	h.jwtHandler.SignInKey = h.cfg.SignInKey
	h.jwtHandler.Log = h.log
	tokens, err := h.jwtHandler.GenerateAuthJWT()
	accessToken := tokens[0]

	if err != nil {
		h.log.Error("error occured while generating tokens")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong,please try again",
		})
		return
	}

	res.AccessToken = accessToken
	res.Password = ""

	c.JSON(http.StatusOK, res)
}
