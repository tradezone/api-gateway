package v1

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/tradezone/api-gateway/api/models"
	"gitlab.com/tradezone/api-gateway/genproto/customer"
	"gitlab.com/tradezone/api-gateway/pkg/logger"
	"net/http"
	"strconv"
	"time"

	"github.com/gin-gonic/gin"
	"github.com/spf13/cast"
	"google.golang.org/protobuf/encoding/protojson"
)

// Verify customer
// @Summary      Verify customer
// @Description  Verifys customer
// @Tags         Customer
// @Accept       json
// @Produce      json
// @Param        email  path string true "email"
// @Param        code   path string true "code"
// @Success      200  {object}  customer.CustomerRes
// @Router      /verify/{email}/{code} [get]
func (h *handlerV1) Verification(c *gin.Context) {
	var jspbMarshal protojson.MarshalOptions
	jspbMarshal.UseProtoNames = true
	var (
		code  = c.Param("code")
		email = c.Param("email")
	)
	customerBody, err := h.inMermoryStorage.Get(email)
	if err != nil {
		c.JSON(http.StatusGatewayTimeout, gin.H{
			"info":  "Your time has expired",
			"error": err.Error(),
		})
		h.log.Error("Error while getting customer from redis", logger.Any("redis", err))
		return
	}
	body := customer.CustomerReq{}
	csBodys := cast.ToString(customerBody)
	err = json.Unmarshal([]byte(csBodys), &body)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while unmarshaling from json to customer body", logger.Any("json", err))
		return
	}
	if body.Code != code {
		fmt.Println(body.Code)
		c.JSON(http.StatusConflict, gin.H{
			"info": "Wrong code",
		})
		return
	}
	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	res, err := h.serviceManager.Customer().CreateCustomer(ctx, &body)
	fmt.Println("creating user in verify: ", err)
	if err != nil {
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": err.Error(),
		})
		h.log.Error("Error while creating customer in verification", logger.Any("post", err))
		return
	}
	response := &models.VerifyResponse{
		Id:          res.Id,
		FirstName:   res.FirstName,
		LastName:    res.LastName,
		Email:       res.Email,
		Bio:         res.Bio,
		PhoneNumber: res.PhoneNumber,
	}
	for _, add := range res.Addresses {
		response.Addresses = append(response.Addresses, models.AddressResponse{
			Id:       add.Id,
			UserId:   add.UserId,
			District: add.District,
			Street:   add.Street,
		})
	}
	//generating refresh and jwt tokens
	// fmt.Println(res)
	h.jwtHandler.Iss = res.FirstName
	h.jwtHandler.Sub = strconv.Itoa(int(res.Id))
	h.jwtHandler.Role = "user"
	h.jwtHandler.Aud = []string{"exam-app"}
	h.jwtHandler.SignInKey = h.cfg.SignInKey
	data, _ := json.MarshalIndent(h.jwtHandler, "", "    ")
	fmt.Println(string(data))
	tokens, err := h.jwtHandler.GenerateAuthJWT()
	if err != nil {
		h.log.Error("error occured while generating tokens")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong, please try again",
		})
		return
	}
	accessToken := tokens[0]
	refreshToken := tokens[1]

	response.JWT = accessToken
	response.RefreshToken = refreshToken

	c.JSON(http.StatusOK, response)
}
