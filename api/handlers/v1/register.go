package v1

import (
	"context"
	"encoding/json"
	"fmt"
	"gitlab.com/tradezone/api-gateway/api/models"
	"gitlab.com/tradezone/api-gateway/email"
	pc "gitlab.com/tradezone/api-gateway/genproto/customer"
	"gitlab.com/tradezone/api-gateway/pkg/etc"
	"gitlab.com/tradezone/api-gateway/pkg/logger"
	"gitlab.com/tradezone/api-gateway/pkg/utils"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/gin-gonic/gin"
	"golang.org/x/crypto/bcrypt"
)

// register customer
// @Summary 		register customer
// @Description 	this registers customer
// @Tags 			Customer
// @Accept 			json
// @Produce         json
// @Param           registercustomer       body  	models.CustomerRes true "customer"
// @Success         201					  {object} 	models.Error
// @Failure         500                   {object}  models.Error
// @Router          /register [post]
func (h *handlerV1) RegisterCustomer(c *gin.Context) {
	var body pc.CustomerReq

	err := c.ShouldBindJSON(&body)
	if err != nil {
		c.AbortWithStatusJSON(http.StatusBadRequest, models.Error{
			Error: err,
		})
		h.log.Error("Error while binding json", logger.Any("json", err))
		return
	}
	_, err = utils.IsValidMail(body.Email)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "invalid email address",
		})
		return
	}

	body.Email = strings.TrimSpace(body.Email)
	body.Email = strings.ToLower(body.Email)

	body.Password, err = etc.HashPassword(body.Password)
	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Error: err,
		})
		h.log.Error("couldn't hash the password")
		return
	}

	ctx, cancel := context.WithTimeout(context.Background(), time.Second*time.Duration(h.cfg.CtxTimeout))
	defer cancel()

	exists, err := h.serviceManager.Customer().CheckField(ctx, &pc.CheckFieldReq{
		Field: "email",
		Value: body.Email,
	})
	fmt.Println(err)
	if err != nil {
		h.log.Error("error while checking username existance", logger.Error(err))
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "error while cheking username existance",
		})
		return
	}
	if exists.Exists {
		c.JSON(http.StatusOK, gin.H{
			"message": "such username already exists",
		})
		return
	}
	hashPass, err := bcrypt.GenerateFromPassword([]byte(body.Password), 10)
	if err != nil {
		h.log.Error("error while hashing password", logger.Error(err))
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong",
		})
		return
	}
	body.Password = string(hashPass)
	body.Code = strconv.Itoa(utils.RandomNum(6))

	jsNewCustomer, err := json.Marshal(body)
	if err != nil {
		h.log.Error("error while marshaling new user, inorder to insert it to redis", logger.Error(err))
		c.JSON(http.StatusBadRequest, gin.H{
			"error": "error while creating user",
		})
		return
	}
	fmt.Println(string(jsNewCustomer))

	if err = h.inMermoryStorage.SetWithTTl(body.Email, string(jsNewCustomer), 300); err != nil {
		// fmt.Println(err)
		h.log.Error("error while inserting new user into redis")
		c.JSON(http.StatusInternalServerError, gin.H{
			"error": "something went wrong, please try again",
		})
	}

	msg := "Subject: Exam email verification\n Your verification code: " + body.Code
	err = email.SendEmail([]string{body.Email}, []byte(msg))

	if err != nil {
		c.JSON(http.StatusInternalServerError, models.Error{
			Error:       nil,
			Code:        http.StatusAccepted,
			Description: "Your Email is not valid, Please recheck it",
		})
		return
	}

	c.JSON(http.StatusAccepted, gin.H{
		"info": "Your request succesfulley created, YourCode is : " + body.Code,
	})

}
