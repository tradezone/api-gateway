package services

import (
	"fmt"

	"gitlab.com/tradezone/api-gateway/config"
	pc "gitlab.com/tradezone/api-gateway/genproto/customer"

	pe "gitlab.com/tradezone/api-gateway/genproto/email"
	ps "gitlab.com/tradezone/api-gateway/genproto/post"
	pr "gitlab.com/tradezone/api-gateway/genproto/review"

	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

type IServiceManager interface {
	Customer() pc.CustomerServiceClient
	Review() pr.ReviewServiceClient
	Post() ps.PostServiceClient
	Email() pe.EmailServiceClient
}

type serviceManager struct {
	reviewService  pr.ReviewServiceClient
	customerServie pc.CustomerServiceClient
	postService    ps.PostServiceClient
	emailService   pe.EmailServiceClient
}

func (s *serviceManager) Customer() pc.CustomerServiceClient {
	return s.customerServie
}

func (s *serviceManager) Review() pr.ReviewServiceClient {
	return s.reviewService
}

func (s *serviceManager) Post() ps.PostServiceClient {
	return s.postService
}

func (s *serviceManager) Email() pe.EmailServiceClient {
	return s.emailService
}
func NewServiceManager(conf *config.Config) (IServiceManager, error) {

	connReview, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf.ReViewServiceHost, conf.ReViewServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}
	connCustomer, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf.CustomerServiceHost, conf.CustomerServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	connPost, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf.PostServiceHost, conf.PostServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}
	connEmail, err := grpc.Dial(
		fmt.Sprintf("%s:%d", conf.EmailServiceHost, conf.EmailServicePort),
		grpc.WithTransportCredentials(insecure.NewCredentials()),
	)
	if err != nil {
		return nil, err
	}

	serviceManager := &serviceManager{
		reviewService:  pr.NewReviewServiceClient(connReview),
		customerServie: pc.NewCustomerServiceClient(connCustomer),
		postService:    ps.NewPostServiceClient(connPost),
		emailService:   pe.NewEmailServiceClient(connEmail),
	}

	return serviceManager, nil
}
