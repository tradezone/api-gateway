pull_submodule:
	git submodule update --init --recursive

submod_up:
	git submodule update --remote --merge

run:
	go run cmd/main.go

create_proto_submodule:
	git submodule add git@gitlab.com:tradezone/protos.git

proto-gen:
	./script/gen-proto.sh

migrate_up:
	migrate -path file://migrations/ -database postgres://postgres:123@localhost:5432/postgres up

migrate_down:
	migrate -path file://migrations/ -database postgres://postgres:123@localhost:5432/postgres down

migrate_force:
	migrate -path file://migrations/ -database postgres://postgres:123@localhost:5432/postgres force 1

swag:
	swag init -g ./api/router.go -o ./api/docs