package redis

import (

	// rd "github.com/go-redis/redis/v8"
	rd "github.com/gomodule/redigo/redis"
	"github.com/spf13/cast"
)

type RedisRepo struct {
	Rdb *rd.Pool
}

func NewRedis(cl *rd.Pool) *RedisRepo {
	return &RedisRepo{
		Rdb: cl,
	}
}

func (r *RedisRepo) SetWithTTl(key, value string, duration int64) error {

	conn := r.Rdb.Get()
	defer conn.Close()

	_, err := conn.Do("SETEX", key, duration, value)
	return err
}

func (r *RedisRepo) Get(key string) (string, error) {
	conn := r.Rdb.Get()
	defer conn.Close()

	tem, err := conn.Do("GET", key)
	res := cast.ToString(tem)
	return res, err
}
