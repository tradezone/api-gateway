package repo

type InMermoryStorageI interface{
	SetWithTTl(key, value string, duration int64) error
	Get(key string) (string, error)
}